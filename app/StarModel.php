<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StarModel extends Model
{
    public $table='star';
    public $timestamps=false;

    function star_product(){
    	return $this->belongsTo("App\ProductModel",'product_id');
    }

}
