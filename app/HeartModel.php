<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeartModel extends Model
{
      
      public $table='heart';
      public $timestamps=false;

      function heart_product_id(){

      	return $this->belongsTo('App\ProductModel','product_id_heart');
      }

	function heart_user(){
		return $this->belongsTo('App\UserModel','user_id');
	}
}
