<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    
    public $table='product';
    public $timestamps=false;
    

    function imageall(){
    	//shateri tvyalnery vercnelu hamar 
    	return $this->hasMany('App\Add_product_img','product_id');
    }

        function user(){
    	//cnoxi tvyalnery
    	return $this->belongsTo('App\UserModel','user_id');
        function heart(){
            return $this->hasMany('App\HeartModel','product_id_heart');
        }
    }
}
