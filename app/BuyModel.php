<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyModel extends Model
{
	public $table='buy';
    public $timestamps=false;

	function buy_user(){
		return $this->belongsTo('App\UserModel','user_id');
	}
 function buy_product(){
 	return $this->belongsTo('App\ProductModel','product_id');
 }
 function imagealll(){
 	return $this->hasMany('App\Add_product_img','product_id');
 }
    
}
