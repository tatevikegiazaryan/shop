<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Session;

use Closure;

class UserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
   
       if(Session::has('user_id'))

        { 


        return $next($request);
           }
        else{
            return redirect('/login');
            // return view("/login");
        }
    }
}
