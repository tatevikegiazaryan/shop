<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\UserModel;
use App\Add_product_img;
use App\ProductModel;
use App\BuyModel;
use App\HeartModel;
use App\StarModel;


use Illuminate\Http\Request;
 

class UserController extends Controller
{ 
  function star(Request $id){
dd($id);
    $my_id=Session::get('user_id');
    $star=StarModel::where(['user_id'=>$my_id,'product_id'=>$id])->first();

    if(empty($star)){
      $star=new StarModel;
      $star->user_id=$my_id;
      $star->product_id=$id;
      $star->save();
    }
    else{
      StarModel::where(['user_id'=>$my_id,'product_id'=>$id])->delete();
    }
  }

  function my_bought_products(){
      $count=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->count();

      $product=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>1])->get();
    return view('/my_bought_products',compact('product','count'));
  }

  function deleteImage(Request $x){
 Add_product_img::where("id",$x->id)->delete();
   
  }
  function detals($id){
  $detals=ProductModel::where("id",$id)->first();
      $count=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->count();
$heart_view=HeartModel::where(['user_id'=>Session::get('user_id'),'product_id_heart'=>$id])->first();
    return view("/shop-detail",compact("detals",'count','heart_view'));
  }
       function logout(){
        Session::flush();
        return redirect('/login');
       }


       // stex middleware er grac

        function shop(){


        $count=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->count();
                  
                 $id = Session::get('user_id');
        
                 $user=UserModel::where("id",$id)->first();

//$products=ProductModel::where("user_id","!=",$id)->get();
                  $products=ProductModel::where("active","1")->get();//berel vorolor apranqnery baci im id ic
// dd($products[0]->image[0]['image
                  
                  
                  return  view("index",compact('user','products','count'));
     
     
      }

      function edit(Request $x){

            $x->validate([
                'name'     => 'required|max:20',
               'surname'  => 'required|max:20',
                'age'      => 'required|numeric|max:200',


            ]);

  UserModel::where("id",Session::get("user_id"))->
            update([
                'name'=>$x->name,
                'surname'=>$x->surname,
                'age'=>$x->age]);


            return back();
         


      }
  function addProduct(){
        $count=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->count();
        
    return view ('/addProduct',compact('count'));
  }

  function add_db_product(Request $x){
    $x->validate([
         'name'     => 'required|max:20',
         'count'  => 'required|max:50',
         'price'      => 'required|max:10',
         'description'  => 'required|max:200',
       
 
    ]);
 
    $product=new ProductModel;
    $product->name=$x->name
    ;
    $product->user_id=Session::get('user_id');
    $product->count=$x->count;
    $product->price=$x->price;
    $product->description=$x->description;
    $product->save();
    // $p=ProductModel();


$id  = $product->id;
if($files=$x->file('images')){
        foreach($files as $file){

    $name=time().$file->getClientOriginalName();
    $file->move('image',$name);
   $add_product_img=new Add_product_img;
    $add_product_img->product_id=$id;
    $add_product_img->image=$name;
    
    $add_product_img->save();
    
        }
        
    }

 

    return redirect("/addProduct")->with('message',"You  added new product Thank YOU");




  }
  function product_edite_delete(Request $x,$id){

            $x->validate([
                'name'      => 'required',
               'description'  => 'required',
                'count'      => 'required',
                'price'      => 'required',
                 

          
            ]);
    
            
            ProductModel::where("id",$id)->update([
                'name'=>$x->name,
                'description'=>$x->description,
                'count'=>$x->count,
                'price'=>$x->price,
                'active'=>0,
              ]);

            if($files=$x->file('images')){
        foreach($files as $file){

    $name=time().$file->getClientOriginalName();
    $file->move('image',$name);
   $add_product_img=new Add_product_img;
    $add_product_img->product_id=$id;
    $add_product_img->image=$name;
    
    $add_product_img->save();
    
        }
    }
return back();    
  }
   function delete_prod($product_id){
    
    
   
    $x=Add_product_img::where("product_id",$product_id)->get();
    if(!empty($x)){
     for($i=0;$i<count($x);$i++){
    
      
 $unlink=$x[$i]["image"];
      
      unlink("image/".$unlink);
     }
       }
     ProductModel::where("id",$product_id)->delete();
    Add_product_img::where("product_id",$product_id)->delete();
   return redirect ("/index")->with('message',"You  deleted  product Why?");



    

   }
   function my_products(){
   $products=ProductModel::where('user_id',Session::get("user_id"))->get();
   // dd($products);
        $count=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->count();
   
     return view("my_products",compact('products','count'));
   }

 function add_cart(Request $x,$product_id){
  $id=Session::get('user_id');
  $x->validate([
    'count'=>'required']);

  $countt=ProductModel::where('id',$product_id)->first();
  ///blxavori qanakic hanum em te qani hat en gnel //asec sa kara lini arnelu vaxt
  // $end_count=$countt['count']-$x->count;
  $total=$x->count*$countt['price'];

  // ProductModel::where('id',$product_id)->update([
  //   'count'=>$end_count,
  //    ]);
  $buy=New BuyModel;

     $buy->user_id=$id;
     $buy->product_id=$product_id;
     $buy->count=$x->count;
     $buy->total=$total;
     $buy->save();
      
      

        $count=BuyModel::where('user_id',$id)->count();


         

         return back()->with('namak','Thank you for buy','total','count');
  
         

 }
 function cart(){
      
  
        $cart=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->get();

        $count=$cart->count();
        

     
  
   return view('/cart',compact('cart','count'));
 }
 function total(Request $x){
  $tottal_db=(int)$x->total_db;
  
  BuyModel::where('product_id',$x->product_id)->update([
   'total'=>$tottal_db,
    'count'=>$x->buy_count
 ]

 );
  

 }
 function heart($product_id){
   $id=Session::get('user_id');
$heart_view=HeartModel::where(['user_id'=>$id,'product_id_heart'=>$product_id])->first();

   if(empty($heart_view)){
   $heart=new HeartModel;
   $heart->product_id_heart=$product_id;
   $heart->user_id=$id;
   $heart->save();
   
   }
  

   else{
       HeartModel::where(['user_id'=>$id,'product_id_heart'=>$product_id])->delete();
    
    }
    return back();




 }
 function heart_view(){
   $count=BuyModel::where(['user_id'=>Session::get('user_id'),'status'=>0])->count();
   $products=HeartModel::Where("user_id",Session::get('user_id'))->get();
   

  return view('/my_heart',compact('count','products'));
 }


  }
 
 