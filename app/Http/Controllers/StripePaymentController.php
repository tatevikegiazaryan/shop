<?php

namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\BuyModel;
use App\ProductModel;
use Session;
use Stripe;

   
class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()

    {
    $buy_product=BuyModel::where('user_id',Session::get('user_id'))->get();
            $sum=0;
            foreach ($buy_product as $key) {
            $sum+=$key['count']*$key->buy_product['price'];
              }
              $summ='aa';
              

    
         return view('stripe',compact('sum'));
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        
   
         // $products=BuyModel::where('user_id',Session::get('user_id'))->get();
         // dd($request);
     
// $sum=0;
// foreach ($products as $product ) {

       
//          $sum+=$product['count']*$product->buy_product['price'];

         $buy_product=BuyModel::where('user_id',Session::get('user_id'))->get();
            $sum=0;
            foreach ($buy_product as $key) {
            $sum+=$key['count']*$key->buy_product['price'];
           
           $count=ProductModel::where('id',$key['product_id'])->first();
           $all_count=$count['count'];
            $a=$all_count-$key['count'];
           
           ProductModel::where('id',$key['product_id'])->update([
            'count'=>$a]);
           BuyModel::where('user_id',Session::get('user_id'))->update([
            'status'=>1]);
           $prod_0_count=ProductModel::where('id',$key['product_id'])->first();

            if($prod_0_count['count']==0){
          ProductModel::where('id',$key['product_id'])->delete();
            }               
              }
 


        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $sum * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com." 
        ]);
  
        Session::flash('success', 'Payment successful!');
          
        return back();
    }
}