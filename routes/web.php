<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 // sa get vorovhetev linki het enq asxatum ete tvyal uneninq post kgreinq
 Route::get("/signup","RegController@index");
 Route::post("/signup",'RegController@signup');
 Route::get("/login",'RegController@login'); 
 Route::post("/login",'RegController@login_form'); 
 Route::get("/index",'UserController@shop')->middleware('checkuser'); 
 Route::get("/admindashboard",'AdminController@admin'); 
 Route::post("/edit_user",'UserController@edit');
 Route::get("/logout",'UserController@logout');
 Route::get("/addProduct","UserController@addProduct")->middleware('checkuser');
 Route::post("/add_db_product",'UserController@add_db_product');
 Route::get("/shop-detail/{id}",'UserController@detals')->middleware('checkuser');
 Route::post("/product_edite_delete/{id}",'UserController@product_edite_delete');
 Route::get("/delete_prod/{prod_id}","UserController@delete_prod");

  Route::post("/deleteImage",'UserController@deleteImage')->middleware('checkuser');
  Route::get("/my_products",'UserController@my_products')->middleware('checkuser');
  // admin
  Route::get("/confirm/{id}",'AdminController@confirm');
  Route::post('/ignor','AdminController@ignor');
  Route::post('/block','AdminController@block');
    Route::get('/hastatum/{email}/{hash}','RegController@userChacke');
    Route::get('/forget','RegController@forget');
    Route::post('/code','RegController@code');
    Route::get('/code','RegController@codeview');
    Route::post('/stugum/{mail}', 'RegController@stugum');
    Route::post('/add_cart/{product_id}',"UserController@add_cart");
    Route::get('/cart','UserController@cart');
    // total poxely db_um
    Route::post('/total','UserController@total');
    Route::get('/heart/{id}','UserController@heart');
 	  Route::get('/my_heart','UserController@heart_view');
    Route::get('/my_bought_products','UserController@my_bought_products');
    Route::post('/star','UserController@star');
//vjarman hamarkarg
  Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');




 
 

       


 // grum en q te inch grecic inch asxati karox e urish ban asxati urish blade asxati 
 // sa nra hamar e er vor vonc bacenq
 // sa petq e controlerum funkcian ngrenq ev uxxaki  anuny t\grelov ktexadrvi aystrex