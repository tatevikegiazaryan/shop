/*
 Navicat Premium Data Transfer

 Source Server         : Tatev
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : tatevshop

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 05/12/2019 01:03:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for add_product_img
-- ----------------------------
DROP TABLE IF EXISTS `add_product_img`;
CREATE TABLE `add_product_img`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of add_product_img
-- ----------------------------
INSERT INTO `add_product_img` VALUES (1, 16, '1574353245about-img.jpg');
INSERT INTO `add_product_img` VALUES (2, 16, '1574353245add-img-01.jpg');
INSERT INTO `add_product_img` VALUES (3, 16, '1574353245apple-touch-icon.png');
INSERT INTO `add_product_img` VALUES (4, 16, '1574353245banner-01.jpg');
INSERT INTO `add_product_img` VALUES (5, 20, '1574365433spoong.png');
INSERT INTO `add_product_img` VALUES (6, 20, '1574365433strawberyy.jpg');
INSERT INTO `add_product_img` VALUES (12, 22, '1574531931instagram-img-05.jpg');
INSERT INTO `add_product_img` VALUES (14, 22, '1574531931instagram-img-07.jpg');
INSERT INTO `add_product_img` VALUES (16, 32, '1574531932instagram-img-05.jpg');
INSERT INTO `add_product_img` VALUES (17, 22, '1574531932instagram-img-06.jpg');
INSERT INTO `add_product_img` VALUES (33, 22, '1574703856spoong.png');
INSERT INTO `add_product_img` VALUES (49, 36, '1575233295spoong.png');
INSERT INTO `add_product_img` VALUES (51, 37, '1575320858a.jpg');
INSERT INTO `add_product_img` VALUES (52, 38, '1575480616Moon.jpg');
INSERT INTO `add_product_img` VALUES (53, 39, '1575480668strawberyy.jpg');
INSERT INTO `add_product_img` VALUES (54, 40, '1575480725minions.jpg');
INSERT INTO `add_product_img` VALUES (55, 41, '1575481425minions.jpg');

-- ----------------------------
-- Table structure for buy
-- ----------------------------
DROP TABLE IF EXISTS `buy`;
CREATE TABLE `buy`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `count` int(11) NULL DEFAULT NULL,
  `total` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of buy
-- ----------------------------
INSERT INTO `buy` VALUES (1, 17, 34, 9, 34);
INSERT INTO `buy` VALUES (8, 17, 35, 9, 435);
INSERT INTO `buy` VALUES (9, 17, 34, 10, 156);
INSERT INTO `buy` VALUES (22, 20, 37, 10, 120);
INSERT INTO `buy` VALUES (23, 20, 38, 10, 2000);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `count` int(11) NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (32, 20, 'vjm', 7822, 687.00, 'hkjh', 'Dirs chi galis');
INSERT INTO `product` VALUES (36, 17, 'pordz_product Spoong', 722, 100.00, 'Spoong Bob Pordz', 'Vatna');
INSERT INTO `product` VALUES (37, 17, 'kj', 646, 12.00, 'qqqqq', '1');
INSERT INTO `product` VALUES (38, 17, 'Moon', 12, 200.00, 'Moon', '1');
INSERT INTO `product` VALUES (41, 17, 'mINION', 20, 120.00, 'Minion', '1');

-- ----------------------------
-- Table structure for tatevshop
-- ----------------------------
DROP TABLE IF EXISTS `tatevshop`;
CREATE TABLE `tatevshop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int(3) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'user',
  `images` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'spoong.jpg',
  `block` int(255) NULL DEFAULT 0,
  `active` int(11) NULL DEFAULT 0,
  `code` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tatevshop
-- ----------------------------
INSERT INTO `tatevshop` VALUES (6, 'a', 'a', 22, 'tatev@mail.ru', '$2y$10$oL6MszqCaI8Dfwrw2DOzSuOUdZF9bxg62lwzAmBvNUJFZx6MCgbJq', 'user', 'spoong.jpg', 1575322524, 0, 0);
INSERT INTO `tatevshop` VALUES (7, 'tt', 'ttt', 44, 'Rart@hjs.ru', '$2y$10$z6mhFxZvWvjUZA5ugzxHcu7ktqsjMx8XetON4s7GpR6ZubVzIAe8u', 'user', 'spoong.jpg', 0, 0, 0);
INSERT INTO `tatevshop` VALUES (8, 'Marine', 'Egiazaryan', 23, 'marine@mail.ru', '$2y$10$tDAsQ6Zj9nZIFzV2T3OhWeL.EEV4F2jtWp6r47itsOMkKmS0Oe.qK', 'user', 'spoong.jpg', 0, 0, 0);
INSERT INTO `tatevshop` VALUES (12, 'ljn', 'fhgc', 4, 'kakal@mail.ru', '$2y$10$hO9agTLKyahWumCqTpxPfOVNu1oGw/fU9ZivI9X7gWjrJq.7050e6', 'user', 'spoong.jpg', 0, 0, 0);
INSERT INTO `tatevshop` VALUES (13, 'nmm', 'bjb', 6, 'kaka@mail.ru', '$2y$10$IA7pYi2v.zl8JYAIsrRbQuDWK5Rfw/4icbJDZ2mG.aZ9zDDvynHbi', 'user', 'spoong.jpg', 1575118530, 0, 0);
INSERT INTO `tatevshop` VALUES (14, 'fh', 'ff', 44, 'kakal@mail.ru', '$2y$10$6a6oc8u.WKpinJ8pH5ertOgCmE69G88XnkcRiswdYcpX6gNKuTRji', 'user', 'spoong.jpg', 0, 0, 0);
INSERT INTO `tatevshop` VALUES (15, 'Karen', 'Kaeapetyan', 22, 'karen@mal.ru', '$2y$10$j/.2k7nA0Ff6DUH5CnLSyOEEnoHpxO5HdZ.JGpDsvl1E/HygPfW3W', 'user', 'spoong.jpg', 0, 0, 0);
INSERT INTO `tatevshop` VALUES (16, 'tatevik', 'Eghiazaryan', 22, 'tatev@mail.ru', '$2y$10$yEZ4R4dHBvnRq2pln5BE4uia4Ip9J3JgHERNjIxfiiDweyFAWFRem', 'admin', 'spoong.jpg', 0, 0, 0);
INSERT INTO `tatevshop` VALUES (17, 'Marine', 'Marine', 56, 'a@mail.ru', '$2y$10$OBveTAjP2r3Hj/UVC/B.a.OmgVFvgs70TrvB0Xblx/TgENnsxWAQa', 'user', 'spoong.jpg', 0, 1, 0);
INSERT INTO `tatevshop` VALUES (18, 'user', 'user', 22, 'tateve9@gmail.com', '$2y$10$zdRlJnMYqSAUpyc4xtGYtOnFI68IR0T0eX99SBaNjBQs/s8IGbdrC', 'user', 'spoong.jpg', 0, 0, 9512);
INSERT INTO `tatevshop` VALUES (20, 'Tatevik', 'Eghiazaryan', 22, 'tatevike9@gmail.com', '$2y$10$SDAm9TD06wWrcTDMEEBWmuSKgDdi8E7vDRb8cLgwbpIL9JFImTRmu', 'user', 'spoong.jpg', 0, 1, 8831);

SET FOREIGN_KEY_CHECKS = 1;
