 <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
                @yield('title')


    <!-- Site Metas -->
  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="shortcut icon" href="{{URL::asset('images/favicon.ico')}}" type="image/x-icon">
          <link rel="apple-touch-icon" href="{{URL::asset('images/apple-touch-icon.png')}}">

       
    <link rel="stylesheet" href="{{URL::asset('css_shop/bootstrap.min.css')}}">

    <!-- Site CSS -->
    <link rel="stylesheet" href="{{URL::asset('css_shop/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{URL::asset('css_shop/responsive.css')}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('css_shop/custom.css')}}">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

<body>
      <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">

                       <li class="nav-item"><a class="nav-link" href="{{URL::to('/index')}}">Home</a></li>
                         <li class="nav-item"><a class="nav-link" href="{{URL::to('/my_bought_products')}}">My bought Products</a></li>
                        <li class="nav-item "><a class="nav-link" href="{{Url::to('/my_products')}}">My Products</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{Url::to('addProduct')}}">Add Product</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{URL::to('/my_heart')}}">My Preffered Products</a></li>
                            <li class="nav-item "><a class="nav-link" href="{{URL::to('/logout')}}">Logout </a></li>
                            
                    </ul>
                </div>
                <!-- /.navbar-collapse -->

                <!-- Start Atribute Navigation -->

                <div class="attr-nav">
                    <ul>
                        <li class="search"><a href="#"> </a></li>
                        <li class="side-menu">
                            <a href="{{URL::to('/cart')}}">
                                
                                 <span>{{$count}}</span> 
                                <p>My Cart </p>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Atribute Navigation -->
            </div>

            <!-- Start Side Menu -->
            <div class="side">
                <a href="#" class="close-side"><i class="fa fa-times"></i></a>
                <li class="cart-box">
                    <ul class="cart-list">
                        <li>
                            <a href="#" class="photo"><img src="images/img-pro-01.jpg" class="cart-thumb" alt="" /></a>
                            <h6><a href="#">Delica omtantur </a></h6>
                            <p>1x - <span class="price">$80.00</span></p>
                        </li>
                        <li>
                            <a href="#" class="photo"><img src="images/img-pro-02.jpg" class="cart-thumb" alt="" /></a>
                            <h6><a href="#">Omnes ocurreret</a></h6>
                            <p>1x - <span class="price">$60.00</span></p>
                        </li>
                        <li>
                            <a href="#" class="photo"><img src="images/img-pro-03.jpg" class="cart-thumb" alt="" /></a>
                            <h6><a href="#">Agam facilisis</a></h6>
                            <p>1x - <span class="price">$40.00</span></p>
                        </li>
                        <li class="total">
                            <a href="#" class="btn btn-default hvr-hover btn-cart">VIEW CART</a>
                            <span class="float-right"><strong>Total</strong>: $180.00</span>
                        </li>
                    </ul>
                </li>
            </div>
            <!-- End Side Menu -->
        </nav>
        <!-- End Navigation -->
    </header>



 

    
          
                <!-- End Header Navigation -->

                @yield('header')

                 <!-- ALL JS FILES -->
    <script src="{{URL::asset('js_shop/jquery-3.2.1.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/popper.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/bootstrap.min.js')}}"></script>

     <script src="{{URL::asset('js_shop/jquery.superslides.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/bootstrap-select.js')}}"></script>
    <script src="{{URL::asset('js_shop/inewsticker.js')}}"></script>
    <script src="{{URL::asset('js_shop/bootsnavr.js')}}"></script>
    <script src="{{URL::asset('js_shop/images-loded.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/isotope.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/owl.carouse.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/baguetteBox.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/form-validator.min.js')}}"></script>
    <script src="{{URL::asset('js_shop/contact-form-script.js')}}"></script>
    <script src="{{URL::asset('js_shop/custom.js')}}"></script>
      <script src="{{URL::asset('js_shop/delete.js')}}"></script>
 
  
    </script>
</body>

</html>

